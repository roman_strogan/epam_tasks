using System;
using System.Text;

namespace Matrix
{
    class Matrix
    {
        // Матриця
        private int[,] _matrix;
        // Розмірність матриці
        private int _m; 
        private int _n;

        // Властивість, яка повертає першу розмірність
        public int FirstDimension
        {
            get
            {
                return _m;
            }
        }
        // Властивість, яка повертає другу розмірність
        public int SecondDimension
        {
            get
            {
                return _n;
            }
        }
        // Перевірка чи об'єкт пустий
        public bool IsEmpty
        {
            get
            {
                return _matrix == null;
            }
        }
        // Конструктор за замовчуванням
        public Matrix()
        {
            this._matrix = new int[0, 0];
        }
        // Конструктор для створення матриці заданого типу
        public Matrix(int m, int n)
        {
            if(m <= 0 || n <= 0)
            return;
 
            this._m = m;
            this._n = n;
            _matrix = new int[m, n];
        }
        // Конструктор копіювання
        public Matrix(Matrix m)
        {
            _m = m._m;
            _n = m._n;
            _matrix = (int[,])m._matrix.Clone();
        }
        // Конструктор для створення об'єкту на основі масиву
        public Matrix(int[,] m)
        {
            _m = m.GetLength(0);
            _n = m.GetLength(1);
            _matrix = (int[,])m.Clone();
        }
       
        // Індексатор для доступу до елементів матриці
        public int this[int i, int j]
        {
            get
            {
                if (i < 0 || i >= _m || j < 0 || j >= _n)
                    throw new IndexOutOfRangeException();

                return _matrix[i, j];
            }

            set
            {
                if (i < 0 || i >= _m || j < 0 || j >= _n)
                    throw new IndexOutOfRangeException();

                _matrix[i, j] = value;
            }
        }
        /// <summary>
        /// Перегрузка оператора додавання
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        /// <returns>Сума двох матриць</returns>
        public static Matrix operator +(Matrix firstMatrix, Matrix secondMatrix)
        {
            // При різних розмірностях матриці повертається пустий об'єкт
            if (firstMatrix._m != secondMatrix._m || firstMatrix._n != secondMatrix._n)
                return new Matrix();

            Matrix sum = new Matrix(firstMatrix);
            for (int i = 0; i < sum._m; i++)
            {
                for (int j = 0; j < sum._n; j++)
                {
                    sum[i, j] = firstMatrix[i,j] + secondMatrix[i, j];
                }
            }
            return sum;
        }
        /// <summary>
        /// Перегрузка оператора віднімання
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        /// <returns>Різниця двох матриць</returns>
        public static Matrix operator -(Matrix firstMatrix, Matrix secondMatrix)
        {
            // При різних розмірностях матриці повертається пустий об'єкт
            if (firstMatrix._m != secondMatrix._m || firstMatrix._n != secondMatrix._n)
                return new Matrix();

            Matrix difference = new Matrix(firstMatrix);
            for (int i = 0; i < difference._m; i++)
            {
                for (int j = 0; j < difference._n; j++)
                {
                    difference[i, j] = firstMatrix[i, j] - secondMatrix[i, j];
                }
            }
            return difference;
        }
        /// <summary>
        /// Перегрузка оператора множення
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        /// <returns>Добуток двох матриць</returns>
        public static Matrix operator *(Matrix firstMatrix, Matrix secondMatrix)
        {
            // При різних розмірностях матриці повертається пустий об'єкт
            if (firstMatrix._n != secondMatrix._m)
                return new Matrix();

            Matrix product = new Matrix(firstMatrix._m, secondMatrix._n);
            for (int i = 0; i < firstMatrix._m; i++)
            {
                for (int j = 0; j < secondMatrix._n; j++)
                {
                    for (int k = 0; k < firstMatrix._n; k++)
                    {
                        product[i, j] += firstMatrix[i, k] * secondMatrix[k, j];
                    }
                }
            }

            return product;
        }
        /// <summary>
        /// Перегрузка оператора множення
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="value"></param>
        /// <returns>Добуток матриці на число</returns>
        public static Matrix operator *(Matrix firstMatrix, int value)
        {
            Matrix product = new Matrix(firstMatrix);
            for (int i = 0; i < firstMatrix._m; i++)
            {
                for (int j = 0; j < firstMatrix._n; j++)
                {
                    product[i, j] = firstMatrix[i,j] * value;
                }
            }

            return product;
        }
        /// <summary>
        /// Транспонування матриці
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <returns>Транспонована матриця</returns>
        public static Matrix Transposition (Matrix firstMatrix)
        {
            Matrix transposition = new Matrix(firstMatrix._m,firstMatrix._n);

            for (int i = 0; i < firstMatrix._m; i++)
            {
                for (int j = 0; j < firstMatrix._n; j++)
                {
                    transposition[i,j] = firstMatrix[j,i];
                }
            }
            return transposition;
        }
        /// <summary>
        /// Перегрузка оператора недорівнює
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        public static bool operator !=(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix._m != secondMatrix._m || firstMatrix._n != secondMatrix._n)
                return true;

            for (int i = 0; i < firstMatrix._m; i++)
            {
                for (int j = 0; j < firstMatrix._n; j++)
                {
                    if (firstMatrix._matrix[i, j] != secondMatrix._matrix[i, j])
                        return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Перегрузка оператора дорівнює
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        public static bool operator ==(Matrix firstMatrix, Matrix secondMatrix)
        {
            return !(firstMatrix != secondMatrix);
        }
        // Перегрузка методу Equals для перевірки рівності об'єктів
        public override bool Equals(object obj)
        {
            try
            {
                return (bool)(this == (Matrix)obj);
            }
            catch
            {
                // Якщо об'єкт не належить до класу Matrix
                return false;
            }
        }
        // Перегрузка методу GetHashCode для повернення хеш-коду об'єкта
        public override int GetHashCode()
        {
            return _m ^ _n;
        }
        /// <summary>
        /// Виведення матриці на екран
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder("", 64);
            for (int i = 0; i < _m ; i++)
            {
                for (int j = 0; j < _n; j++)
                {
                    sb.Append(_matrix[i, j]).Append("\t");
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
       
        public static void Main()
        {
            int[,] a = new int[,] {{ 1, 2, 3 },
                                   { 4, 5, 6 },
                                   { 7, 8, 9 }};

            Matrix m = new Matrix(a);
            Console.WriteLine("Matrix:");
            Console.WriteLine(m);

            Matrix m1 = m + m;
            Console.WriteLine("Sum of the matrixes");
            Console.WriteLine(m1);
           
            Matrix m2 = m - m;
            Console.WriteLine("Difference of the matrixes");
            Console.WriteLine(m2);
           
            Matrix m3 = m * m;
            Console.WriteLine("Product of the matrixes");
            Console.WriteLine(m3);
           
            Matrix m4 = m * 4;
            Console.WriteLine("Product of the matrix and value");
            Console.WriteLine(m4);
           
            m4 = Transposition(m4);
            Console.WriteLine("Transposition of the last matrix");
            Console.WriteLine(m4);
   
            if (m1.IsEmpty)
            {
                Console.WriteLine("Empty object");
            }
            else
            {
                Console.WriteLine("Non empty object");
            }
            if (m1 == m2)
                Console.WriteLine("Equal");
            else
                Console.WriteLine("Not Equal");r
        }
    }
}
